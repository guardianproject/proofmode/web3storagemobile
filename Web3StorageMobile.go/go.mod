module gitlab.com/guardianproject/proofmode/web3storagemobile.git

go 1.16

require (
	github.com/web3-storage/go-w3s-client v0.0.7
	golang.org/x/mobile v0.0.0-20230531173138-3c911d8e3eda // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/tools v0.1.12 // indirect
)
