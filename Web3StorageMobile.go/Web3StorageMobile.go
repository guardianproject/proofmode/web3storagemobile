package Web3StorageMobile

import (
	"context"
	"fmt"
	"io"
	"io/fs"
	"os"

	"github.com/ipfs/go-cid"
	"github.com/web3-storage/go-w3s-client"
	w3fs "github.com/web3-storage/go-w3s-client/fs"
)

func UploadToWeb3(endpoint string, token string, uploadPath string) string {
	c, err := w3s.NewClient(
		w3s.WithEndpoint(endpoint),
		w3s.WithToken(token),
	)
	if err != nil {
		panic(err)
	}

	cid := putSingleFile(c, uploadPath)

        return fmt.Sprint(cid)

	// getStatusForKnownCid(c)
	// getFiles(c)
	// listUploads(c)
}

func GetWeb3Status(endpoint string, token string, cidstring string) string {
        c, err := w3s.NewClient(
                w3s.WithEndpoint(endpoint),
                w3s.WithToken(token),
        )
        if err != nil {
                panic(err)
        }
        
	cid, _ := cid.Parse(cidstring)
        return getStatusForCid(c, cid)
        // getStatusForKnownCid(c)
        // getFiles(c)
        // listUploads(c)
}

func putSingleFile(c w3s.Client, uploadPath string) cid.Cid {
	file, err := os.Open(uploadPath)
	if err != nil {
		panic(err)
	}
	return putFile(c, file)
}

func putMultipleFiles(c w3s.Client) cid.Cid {
	f0, err := os.Open("images/donotresist.jpg")
	if err != nil {
		panic(err)
	}
	f1, err := os.Open("images/pinpie.jpg")
	if err != nil {
		panic(err)
	}
	dir := w3fs.NewDir("comic", []fs.File{f0, f1})
	return putFile(c, dir)
}

func putDirectory(c w3s.Client, dirpath string) cid.Cid {
	dir, err := os.Open(dirpath)
	if err != nil {
		panic(err)
	}
	return putFile(c, dir)
}

func putFile(c w3s.Client, f fs.File, opts ...w3s.PutOption) cid.Cid {
	cid, err := c.Put(context.Background(), f, opts...)
	if err != nil {
		panic(err)
	}
	fmt.Printf("https://%v.ipfs.dweb.link\n", cid)
	return cid
}

func getStatusForCid(c w3s.Client, cid cid.Cid) string {
	s, err := c.Status(context.Background(), cid)
	if err != nil {
		panic(err)
	}
        return fmt.Sprint(s)
}

func getFiles(c w3s.Client, cidstring string) {
	cid, _ := cid.Parse(cidstring)

	res, err := c.Get(context.Background(), cid)
	if err != nil {
		panic(err)
	}

	f, fsys, err := res.Files()
	if err != nil {
		panic(err)
	}

	info, err := f.Stat()
	if err != nil {
		panic(err)
	}

	if info.IsDir() {
		err = fs.WalkDir(fsys, "/", func(path string, d fs.DirEntry, err error) error {
			info, _ := d.Info()
			fmt.Printf("%s (%d bytes)\n", path, info.Size())
			return err
		})
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Printf("%s (%d bytes)\n", cid.String(), info.Size())
	}
}

func listUploads(c w3s.Client) {
	uploads, err := c.List(context.Background())
	if err != nil {
		panic(err)
	}

	for {
		u, err := uploads.Next()
		if err != nil {
			// finished successfully
			if err == io.EOF {
				break
			}
			panic(err)
		}

		fmt.Printf("%s	%s	Size: %d	Deals: %d	Pins: %d\n", u.Created.Format("2006-01-02 15:04:05"), u.Cid, u.DagSize, len(u.Deals), len(u.Pins))
	}
}
