
# Web3Storage Mobile Library

This project makes the Web3.Storage Go Library available to Android and iOS developers to integrate into their apps

https://web3.storage/docs/reference/go-client-library/

The goal is to support all the major API calls, but to start our focus is on uploading and status. This will allow any mobile app, with a "free" web3.storage token provided to it, to upload one or more files to web3.storage, and have them made available over IPFS through the Filecoin network.

## iOS/macOS

### Installation

Web3StorageMobile is not yet available other than this repository.

### Getting Started

## iOS

## Android 

### Installation


#### Security Concerns:

Since it is relatively easy in the Java/Android ecosystem to inject malicious
packages into projects by leveraging the order of repositories and release malicious
versions of packages on repositories which come *before* the original one in the
search order, the only way to keep yourself safe is to explicitly define, which 
packages should be loaded from which repository, when you use multiple repositories:

https://docs.gradle.org/5.1/userguide/declaring_repositories.html#sec::matching_repositories_to_dependencies


### Getting Started

If you are building a new Android application be sure to declare that it uses the
`INTERNET` permission in your Android Manifest:

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="my.test.app">

    <uses-permission android:name="android.permission.INTERNET"/>
    <application ...

```

## Build

### Requirements

This repository contains a precompiled iOS and Android version of Web3StorageMobile.
If you want to compile it yourself, you'll need Go 1.16 as a prerequisite.

You will also need Xcode installed when compiling for iOS and an Android NDK
when compiling for Android.

The build script needs the gomobile binary and will install it, if not available, yet.
However, you'll still need to make it accessible in your `$PATH`.

So, if it's not already, add `$GOPATH/bin` to `$PATH`. The default location 
for `$GOPATH` is `$HOME/go`: 

```shell
export PATH=$HOME/go/bin/:$PATH` 
```

### iOS

Make sure Xcode and Xcode's command line tools are installed. Then run

### Android

Make sure that `javac` is in your `$PATH`. If you do not have a JDK instance, on Debian systems you can install it with: 

```shell
apt install default-jdk 
````

If they aren't already, make sure the `$ANDROID_HOME` and `$ANDROID_NDK_HOME` 
environment variables are set:

```shell
export ANDROID_HOME=~/Android/Sdk
export ANDROID_NDK_HOME=$ANDROID_HOME/ndk/$NDK_VERSION

rm -rf Web3StorageMobile.aar Web3StorageMobile-sources.jar && ./build.sh android
```

This will create an `Web3StorageMobile.aar` file, which you can directly drop in your app, 
if you don't want to rely on Maven Central or JitPack.

On certain CPU architectures `gobind` might fail with this error due to setting
a flag that is no longer supported by Go 1.16:

```
go tool compile: exit status 1
unsupported setting GO386=387. Consider using GO386=softfloat instead.
gomobile: go build -v -buildmode=c-shared -o=/tmp/gomobile-work-855414073/android/src/main/jniLibs/x86/libgojni.so ./gobind failed: exit status 1
```

If this is the case, you will need to set this flag to build IPtProxy:

```shell
export GO386=sse2
```

## Authors

- Nathan Freitas n8fr8 nathan@guardianproject.info

for the Guardian Project https://guardianproject.info

## License

Web3StorageMobile is available under the MIT license. See the [LICENSE](LICENSE) file for more info.
